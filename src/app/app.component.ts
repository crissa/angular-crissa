import { Component } from '@angular/core';
import { Message } from './message';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = '留言版_crissa';
  name=''; //名稱
  content=''; //內容
  messages: Message[] = [];
  addmsg(){
    if(this.name.trim()||this.content.trim()){ //if不為空
    const message=new Message(this.name,this.content); //創建一個message
    this.messages.push(message); //將message推到
    this.content = '';
    }
  }
}
